AUL (Arrays Utils Library)

METHOD SUMMARY
==============
1static double[] append(double[] a, double value)
	Returns an array with the same elements of a but with value added to the end of the array.
static int binarySearch(double[] a, double key)
	Searches the specified array of doubles for the specified value using the binary search algorithm.
static int binarySearch(double[] a, int fromIndex, int toIndex, double key)
	Searches a range of the specified array of doubles for the specified value using the binary search algorithm.
static double[]	copyOf(double[] original, int newLength)
	Copies the specified array, truncating or padding with zeros (if necessary) so the copy has the specified length.
static double[]	copyOfRange(double[] original, int from, int to)
	Copies the specified range of the specified array into a new array.
static boolean equals(double[] a, double[] a2)
	Returns true if the two specified arrays of doubles are equal to one another.
static void fill(double[] a, double val)
	Assigns the specified double value to each element of the specified array of doubles.
static void fill(double[] a, int fromIndex, int toIndex, double val)
	Assigns the specified double value to each element of the specified range of the specified array of doubles.
static double[] insert(double[] a, int index, double value)
	Returns an array with the same element of a but with value inserted in index.
static double[] union(double[] a, double[] b)
	Returns an array with all elements of a and, after them, all elements of b.
static double[] prepend(double[] a, double value)
	Returns an array with the same elements of a but with value added to the beginning of the array.
static double[] remove(double[] a, int index)
	Returns an array with the same elements of a but without value in index.
static void sort(double[] a)
	Sorts the specified array into ascending numerical order.
static void sort(double[] a, int fromIndex, int toIndex)
	Sorts the specified range of the array into ascending order.
static String toString(double[] a)
	Returns a string representation of the contents of the specified array.

Imagine 5 methods more that you like to have in an arrays library!
