/*
 * ArraysUtilsDemo.java        1.0 01/02/2016
 *
 * Demo of Arrays Utils Library.
 */

package AUL.aul.src.aul;
import java.util.Arrays;

/**
 * Demo of European Date Utils Library.
 */
public class ArraysUtilsDemo {
    
    /**
     * Demo.
     * @param args Not used.
     */
    public static void main(String[] args) {
        System.out.println("WELLCOME ARRAYS UTILS LIBRARY DEMO\nCopyright Joan Garcias <jgarciasc@hotmail.com");
        System.out.println("================================");
        double[] dem = ArraysUtils.create(4);
        dem = ArraysUtils.append(dem,1);
        System.out.printf("%s",ArraysUtils.toString(dem));
    }
}
