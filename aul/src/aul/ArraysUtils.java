/*
 * ArraysUtils.java        1.0 03/01/2016
 *
 * Library with utilities for working with arrays.
 *
 */

package AUL.aul.src.aul;

/**
 * Library with utilities for working with arrays.
 */
public class ArraysUtils {
    
    /**
     * Creates an array.
     * 
     * @param int the length of the new array
     * @return double[]
     */
    public static double[] create(int length) {
        double[] arr = new double[length];
        return arr;
    }
    /*
     * Anyade un elemento al array.
     * @param double[] array
     * @param double value to add
     * @return double[] the array with value added to the end
     */
    static double[] append(double[] a, double value) {
        //Augmentar tamanyo de arr
        double[] b = new double[a.length+1];
        //Grabar datos en arr
        for (int i = 0; i < a.length; i++) {
            b[i] = a[i];
        }
        //Grabar ultimo dato
        b[a.length] = value;
        return b;
    }
    /*
     *  Searches a range of the specified array of doubles for the specified value using the binary
     * @param a an array
     * @param fromIndex
     * @param toIndex
     * @param key
     * @return index
     */
    static int binarySearch(double[] a, int fromIndex, int toIndex, double key) {
        //variable declaration
        boolean found = false;
        int low = fromIndex;
        int high = toIndex;
        int test = 0;
        int index = -1;
        //calcul
        while (low + 1 < high && !found) {
            test = (low + high) / 2;
            if (a[test] > key) {
                high = test;
            }
            else if (a[test] == key) {
                index = test;
                found = true;
            }
            else {
                low = test;
            }
        }
        if (a[low] == key) {
            index = low;
        }
        return index;
    }
    /*
     *  Copies the specified range of the specified array into a new array.
     * @param original the original array
     * @param from start of range
     * @param to end of range
     */
    static double[] copyOfRange(double[] original, int from, int to) {
        //vars
        double[] array = new double[to - from];
        //calc
        for (int i = 0; i < array.length; i++) {
           array[i] = original[from + i];
        }
        //ret
        return array;
    }
    /**
     * Convierte array en String
     * @param double[] an array
     * @return String
     */
    public static String toString(double[] arr){
          String ret = "{";
          for(int i = 0; i < arr.length; i++){
                ret += arr[i] + " ";
          }
          return ret + "}";
    }
}
